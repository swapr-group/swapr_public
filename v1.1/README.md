1 - Load MOOSE:

  * Load a compatible Moose.lua (include version) in your mission, using a MISSION START trigger
    (You can use Moose_develop_late_August_2019.lua, provided in SWAPR_PnP_Hybrid_v1_1.rar)


2 - Configure SWAPR_PnP_Hybrid_v1_1.lua and load it:
   
    https://i.imgur.com/uC8ii6j.jpg

   * Add the relevant client prefixes / suffixes to the SWAPR_Prefixes table  (they must be in string format and separated by , ) 

    
   * Give Game_Mode the value "SP" (solo) or "MP" (multiplayer), as required.   
     If you set it to "SP", then the script will be ready to go!
     If you set it to "MP", you'll need to use SWAPR_Server_Hook.lua as well! 

   * Give Replacement_Type the value "AI" (uncontrolled AI replacements) or "Static" (static replacements), as desired.   
     If set to "AI", clients based on FARPs, open terrain and open airbase parking spots will receive uncontrolled AI replacements. 
     If set to "Static", the mentioned clients will receive static replacements instead 
   
   * Set Hidden_AI_Replacements to either true (enabled) or false (disabled), as desired.   
     If true, generated AI replacements will be hidden on F10 map (doesn't work for static replacements, only works for AI replacements) 
  
   * Set Sheltered_Replacements to either true (enabled) or false (disabled), as desired.   
     If true, SWAPR will generate uncontrolled AI replacements for clients located inside sheltered parking spots. 
     If false, no replacements will be generated for sheltered clients
   
   * Set Ship_Replacements to either true (enabled) or false (disabled), as desired.   
     If true, SWAPR will generate uncontrolled AI replacements for clients based on compatible carriers, destroyers, frigates and cruisers. 
     If false, no replacements will be generated for ship-based clients
   

   * Load SWAPR_PnP_Hybrid_v1_1.lua in your mission, using a ONCE trigger (no conditions required) 


3 - Set up SWAPR_Server_Hook.lua and add it to Hooks (for dedicated server only):

    https://i.imgur.com/zgvn1RX.jpg

    
   * Add the relevant client prefixes / suffixes to the SWAPR_Prefixes table  (they must be in string format and separated by , ) 

    
   * Put SWAPR_Server_Hook.lua in C:\ Users \ USERNAME \ Saved Games \ DCS.openbeta \ Scripts \ Hooks  (this must be done in your dedicated server!)
