1 - Load MOOSE:

* Load a compatible Moose.lua (include version) in your mission, using a MISSION START trigger
  (You can use the included Moose_develop_August_2019.lua)


2 - Set up SWAPR_PnP_Hybrid.lua and load it:
   
    https://i.imgur.com/QqBSULw.jpg

* Add the relevant client prefixes / suffixes to the SWAPR_Prefixes table  (they must be in string format and separated by , ) 

    
* Give Game_Mode the value "SP" (solo) or "MP" (multiplayer), as required.   
  ( If you set it to "MP", you'll need to use SWAPR__Server_Hook_.lua as well! ) 



* Load SWAPR_PnP_Hybrid.lua in your mission, using a ONCE trigger (no conditions required) 


3 - Set up SWAPR_Server_Hook.lua and add it to Hooks (in your dedicated server):

    https://i.imgur.com/zgvn1RX.jpg

    
* Add the relevant client prefixes / suffixes to the SWAPR_Prefixes table  (they must be in string format and separated by , ) 

    
* Put SWAPR_Server_Hook.lua in C:\ Users \ USERNAME \ Saved Games \ DCS.openbeta \ Scripts \ Hooks   (this must be done in your dedicated server!)
